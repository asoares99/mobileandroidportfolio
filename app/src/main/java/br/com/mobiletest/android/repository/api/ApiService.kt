package br.com.mobiletest.android.repository.api

import br.com.mobiletest.android.repository.model.SkyModel
import retrofit.Call
import retrofit.http.GET

interface ApiService{

    companion object {
        val URL_BASE = "https://sky-exercise.herokuapp.com/"
    }

    @GET("api/Movies")
    fun getMovies() : Call<MutableList<SkyModel>>


}