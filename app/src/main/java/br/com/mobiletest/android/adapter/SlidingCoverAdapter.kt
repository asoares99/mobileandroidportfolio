package br.com.mobiletest.android.adapter

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.mobiletest.android.R
import br.com.mobiletest.android.repository.api.ServicePicasso
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_cover_sliding_adapter.view.*

class SlidingCoverAdapter (private val context: Context, private val urlCoverMovieList: List<String>) : PagerAdapter() {
    private var inflater: LayoutInflater? = null
    private var picasso : Picasso? = null

    init {
        inflater = LayoutInflater.from(context)
        picasso = Picasso.Builder(context).downloader(ServicePicasso.getClientPicasso(context)).build()
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }


    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val viewCoverDetails = inflater!!.inflate(R.layout.item_cover_sliding_adapter, view, false)!!

        val imgCover = viewCoverDetails.ivCoverMovieDetails

        picasso!!.load(urlCoverMovieList[position])
                .placeholder(R.drawable.ic_default)
                .into(imgCover)

        view.addView(viewCoverDetails, 0)

        return viewCoverDetails
    }


    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return urlCoverMovieList.size
    }

}