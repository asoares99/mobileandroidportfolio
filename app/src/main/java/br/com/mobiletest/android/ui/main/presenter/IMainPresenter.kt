package br.com.mobiletest.android.ui.main.presenter

import br.com.mobiletest.android.repository.model.SkyModel
import br.com.mobiletest.android.ui.main.IMainView

interface IMainPresenter {
    fun setView(mView : IMainView)
    fun getMovies()
    fun returnMovie(position : Int) : SkyModel
}