package br.com.mobiletest.android.ui.fragment

import android.app.Fragment
import android.os.Bundle
import android.os.Handler
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.mobiletest.android.R
import br.com.mobiletest.android.adapter.SlidingCoverAdapter
import br.com.mobiletest.android.constant.Constant
import br.com.mobiletest.android.repository.model.SkyModel
import kotlinx.android.synthetic.main.fragment_details_movie.view.*
import java.util.*

class DetailsMovieFragment : Fragment() {
    private var movieModel: SkyModel? = null
    private val swipeTimer = Timer()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            movieModel = it.getSerializable(Constant.ARG_PARAM_MOVIE) as SkyModel
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val mView = inflater!!.inflate(R.layout.fragment_details_movie, container, false)
        initWidgets(mView)
        return mView
    }

    fun initWidgets(mView: View) {
        mPager = mView.pager
        mPager!!.adapter = SlidingCoverAdapter(activity, movieModel!!.backdrops)
        NUM_PAGES = movieModel!!.backdrops.size

        val indicator = mView.indicator
        indicator.setViewPager(mPager)

        val density = resources.displayMetrics.density
        indicator.radius = 5 * density

        val handler = Handler()
        val Update = Runnable {
            if (currentPage == NUM_PAGES) {
                currentPage = 0
            }
            mPager!!.setCurrentItem(currentPage++, true)
        }

        swipeTimer.schedule(object : TimerTask() {
            override fun run() {
                handler.post(Update)
            }
        }, 7000, 7000)

        mView.tvTitleNameMovieDetails.text = movieModel!!.title.toString()
        mView.tvLabelYearMovieDetails.text = movieModel!!.year.toString()
        mView.tvLabelDurationMovieDetails.text = movieModel!!.duration.toString()
        mView.tvSinopseMovieDetails.text = getString(R.string.label_description, movieModel!!.description.toString())

    }


    override fun onDetach() {
        super.onDetach()
        mPager  = null
        currentPage = 0
        NUM_PAGES = 0
        swipeTimer.cancel()
    }


    companion object {
        @JvmStatic
        fun newInstance(movie: SkyModel) =
                DetailsMovieFragment().apply {
                    arguments = Bundle().apply {
                        putSerializable(Constant.ARG_PARAM_MOVIE, movie)
                    }
                }

        private var mPager: ViewPager? = null
        private var currentPage = 0
        private var NUM_PAGES = 0
    }
}

