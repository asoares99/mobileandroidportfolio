package br.com.mobiletest.android.adapter

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.mobiletest.android.R
import br.com.mobiletest.android.callback.OnReturnClickMovieListener
import br.com.mobiletest.android.repository.api.ServicePicasso
import br.com.mobiletest.android.repository.model.SkyModel
import kotlinx.android.synthetic.main.item_movie_adapter.view.*
import com.squareup.picasso.Picasso



class MoviesAdapter : RecyclerView.Adapter<MoviesAdapter.ViewHolder>(){
    private var listMovies : List<SkyModel>? = null
    private var picasso : Picasso? = null
    private var listener : OnReturnClickMovieListener? = null

    open fun initAdapter(activity : Activity ,listMovies : List<SkyModel>, listener : OnReturnClickMovieListener) : MoviesAdapter{
        this.listMovies = listMovies
        this.listener = listener
        picasso = Picasso.Builder(activity).downloader(ServicePicasso.getClientPicasso(activity)).build()
        return this
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movies = listMovies!![position]
        if (movies != null){
            picasso!!.load(movies.cover)
                    .placeholder(R.drawable.ic_default)
                    .into(holder.ivCover)
            holder.tvNameMovie.text = movies.title
        }

        holder.ivCover.setOnClickListener{
            listener!!.returnClickItem(position)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_movie_adapter, parent, false)


        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listMovies!!.size
    }


    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view){
        var ivCover = view.ivCoverMovie
        var tvNameMovie = view.tvLabelNameMovie
    }
}