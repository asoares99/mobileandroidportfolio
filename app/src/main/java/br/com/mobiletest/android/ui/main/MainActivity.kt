package br.com.mobiletest.android.ui.main

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.Toolbar
import br.com.mobiletest.android.R
import br.com.mobiletest.android.callback.OnRefreshListMoviesListener
import br.com.mobiletest.android.callback.OnReturnClickMovieListener
import br.com.mobiletest.android.constant.Constant
import br.com.mobiletest.android.repository.model.SkyModel
import br.com.mobiletest.android.ui.fragment.DetailsMovieFragment
import br.com.mobiletest.android.ui.fragment.ListMoviesFragment
import br.com.mobiletest.android.ui.main.presenter.IMainPresenter
import br.com.mobiletest.android.ui.main.presenter.MainPresenterImpl
import br.com.mobiletest.android.util.AlertError
import br.com.mobiletest.android.util.ProgressLoading
import kotlinx.android.synthetic.main.activity_main.*



class MainActivity : AppCompatActivity(), IMainView, OnRefreshListMoviesListener, OnReturnClickMovieListener {
    private var presenter: IMainPresenter? = null
    private var alertProgressLoading : ProgressLoading? = null
    private var toolbar : Toolbar? = null

    override fun getContext(): Context {
        return this
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initToolbar()
        alertProgressLoading = ProgressLoading(this)
        alertProgressLoading!!.show(getString(R.string.label_loading))

        presenter = MainPresenterImpl()
        presenter?.setView(this)
        presenter?.getMovies()
    }

    fun initToolbar(){
        toolbar = this.toolbarBarMovies
        toolbar!!.title = getString(R.string.app_name)
        toolbar!!.setTitleTextColor(ContextCompat.getColor(this,android.R.color.white))
        setSupportActionBar(toolbar)
    }

    override fun refreshList() {
        presenter!!.getMovies()
    }


    override fun returnMovies(listMovies: MutableList<SkyModel>?) {
        val fragmentListMovies = ListMoviesFragment.newInstance(listMovies!!)
        fragmentListMovies.setListener(this)
        fragmentManager.beginTransaction()
                .add(R.id.frameLayoutFragments, fragmentListMovies, Constant.TAG_FRAGMENT_LIST_MOVIES)
                .commit()
        alertProgressLoading!!.dismissloading()
    }

    override fun returnClickItem(position: Int) {
        val fragmentDetailsMovie = DetailsMovieFragment.newInstance(presenter!!.returnMovie(position))
        fragmentManager.beginTransaction()
                .add(R.id.frameLayoutFragments, fragmentDetailsMovie, Constant.TAG_FRAGMENT_DETAILS_MOVIE)
                .addToBackStack(Constant.TAG_FRAGMENT_LIST_MOVIES)
                .commit()
    }

    override fun returnErro(messageError: String) {
        alertProgressLoading!!.dismissloading()
        AlertError(this).show(messageError)
    }
}
