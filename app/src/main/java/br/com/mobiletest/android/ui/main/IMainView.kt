package br.com.mobiletest.android.ui.main

import android.content.Context
import br.com.mobiletest.android.repository.model.SkyModel

interface IMainView {
    fun getContext() : Context
    fun returnMovies(listMovies : MutableList<SkyModel>?)
    fun returnErro(messageError : String)
}