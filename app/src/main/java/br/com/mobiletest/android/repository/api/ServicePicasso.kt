package br.com.mobiletest.android.repository.api

import android.content.Context
import com.squareup.okhttp.Cache
import com.squareup.picasso.OkHttpDownloader
import com.squareup.okhttp.Interceptor
import com.squareup.okhttp.OkHttpClient
import com.squareup.okhttp.Response


class ServicePicasso{
    companion object {
        fun getClientPicasso(context: Context): OkHttpDownloader {
            val okHttpClient = OkHttpClient()
            okHttpClient.networkInterceptors().add(object : Interceptor{
                override fun intercept(chain: Interceptor.Chain?): Response {
                    val originalResponse = chain?.proceed(chain.request())
                    return originalResponse?.newBuilder()!!.header("Cache-Control", "max-age=" + 60 * 60 * 24 * 365).build()
                }
            })
            okHttpClient.cache = Cache(context.cacheDir, Long.MAX_VALUE)
            val okHttpDownloader = OkHttpDownloader(okHttpClient)


            return okHttpDownloader
        }
    }
}