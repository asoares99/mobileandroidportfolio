package br.com.mobiletest.android.util

import android.app.Activity
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.widget.ProgressBar
import br.com.mobiletest.android.R
import kotlinx.android.synthetic.main.activity_base_loading.view.*


class ProgressLoading(activity: Activity) : AlertDialog(activity) {
    private var progressBar: ProgressBar? = null
    private var alertProgress: AlertDialog? = null
    private var activity = activity

    open fun show(message : String){
        var layoutInflater: LayoutInflater = activity!!.layoutInflater
        var view: View = layoutInflater.inflate(R.layout.activity_base_loading, null)
        view.tvMessageLoading.text = message

        progressBar = view.progress_bar_default
        progressBar!!.indeterminateDrawable.setColorFilter(ContextCompat.getColor(activity, R.color.colorPrimaryDark), android.graphics.PorterDuff.Mode.MULTIPLY)

        var builder: AlertDialog.Builder = AlertDialog.Builder(activity)
        builder.setCancelable(false)
        builder.setView(view)
        alertProgress = builder.create()
        alertProgress!!.show()
    }

    open fun dismissloading(){
        alertProgress?.dismiss()
    }
}