package br.com.mobiletest.android.repository.api

import retrofit.GsonConverterFactory
import retrofit.Retrofit


object ConfigRetrofit{

    fun initRetrofit() : Retrofit {
        val retrofit = Retrofit.Builder()
                .baseUrl(ApiService.URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                        .build()

        return retrofit
    }
}