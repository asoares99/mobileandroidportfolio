package br.com.mobiletest.android.ui.main.presenter

import br.com.mobiletest.android.R
import br.com.mobiletest.android.repository.api.ApiService
import br.com.mobiletest.android.repository.api.ConfigRetrofit
import br.com.mobiletest.android.repository.model.SkyModel
import br.com.mobiletest.android.ui.main.IMainView
import br.com.mobiletest.android.util.Utils
import retrofit.Call
import retrofit.Callback
import retrofit.Response
import retrofit.Retrofit

class MainPresenterImpl : IMainPresenter {
    private var mView: IMainView? = null
    private var listMovies : List<SkyModel>? = null

    override fun setView(mView: IMainView) {
        this.mView = mView
    }

    override fun getMovies() {
        if (Utils.checkInternetConnection(mView!!.getContext())){
            val retrofit = ConfigRetrofit.initRetrofit()
            val apiService: ApiService = retrofit.create(ApiService::class.java)
            val call: Call<MutableList<SkyModel>> = apiService.getMovies()
            call.enqueue(object : Callback<MutableList<SkyModel>> {
                override fun onResponse(response: Response<MutableList<SkyModel>>?, retrofit: Retrofit?) {
                    if (response!!.isSuccess){
                        var response = response!!.body()
                        mView?.returnMovies(response)
                        listMovies = response
                    }else{
                        mView?.returnErro(response.message())
                    }
                }

                override fun onFailure(t: Throwable?) {
                    mView!!.returnErro(mView!!.getContext().getString(R.string.alert_message_error_generic))
                }
            })
        }else{
            mView!!.returnErro(mView!!.getContext().getString(R.string.alert_message_error_internet))
        }
    }

    override fun returnMovie(position: Int): SkyModel {
       return listMovies!![position]
    }

}