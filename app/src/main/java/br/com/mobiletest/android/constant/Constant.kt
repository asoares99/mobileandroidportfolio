package br.com.mobiletest.android.constant

internal class Constant{
    companion object {
        //MainActivity
        const val ARG_PARAM_LIST_MOVIE = "paramList"
        const val ARG_PARAM_MOVIE = "paramMovie"

        //Fragments
        const val TAG_FRAGMENT_LIST_MOVIES = "listMovies"
        const val TAG_FRAGMENT_DETAILS_MOVIE = "detailsMovie"
    }
}