package br.com.mobiletest.android.util

import android.app.Activity
import android.support.v7.app.AlertDialog
import br.com.mobiletest.android.R


class AlertError(activity : Activity) : AlertDialog(activity){
    private var activity = activity
    open fun show(message : String){
       AlertDialog.Builder(activity)
                .setTitle(activity.getString(R.string.alert_title_error))
                .setMessage(message)
                .setCancelable(false)
                .setNeutralButton("Ok", null)
                .create()
                .show()
    }
}