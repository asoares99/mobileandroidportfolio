package br.com.mobiletest.android.repository.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class SkyModel: Serializable{
    @SerializedName("title")
    var title: String? = null
    @SerializedName("overview")
    var description: String? = null
    @SerializedName("duration")
    var duration: String? = null
    @SerializedName("release_year")
    var year: String? = null
    @SerializedName("cover_url")
    var cover: String? = null
    @SerializedName("backdrops_url")
    var backdrops: MutableList<String> = mutableListOf()
    @SerializedName("id")
    var id: String? = null





}