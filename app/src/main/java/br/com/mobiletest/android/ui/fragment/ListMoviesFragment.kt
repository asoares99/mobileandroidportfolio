package br.com.mobiletest.android.ui.fragment

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.mobiletest.android.R
import br.com.mobiletest.android.constant.Constant
import br.com.mobiletest.android.repository.model.SkyModel
import br.com.mobiletest.android.adapter.MoviesAdapter
import java.io.Serializable
import android.support.v4.content.ContextCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.*
import br.com.mobiletest.android.callback.OnRefreshListMoviesListener
import br.com.mobiletest.android.ui.main.MainActivity
import kotlinx.android.synthetic.main.fragment_movies.view.*


class ListMoviesFragment : Fragment() {
    private var listMoveis: List<SkyModel>? = null
    private var rvList: RecyclerView? = null
    private var adapter: MoviesAdapter? = null
    private var listener: OnRefreshListMoviesListener? = null
    private var swipeLayout: SwipeRefreshLayout? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            listMoveis = it.getSerializable(Constant.ARG_PARAM_LIST_MOVIE) as List<SkyModel>
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val mView = inflater!!.inflate(R.layout.fragment_movies, container, false)
        initSwipeRefresh(mView)

        initWidgets(mView)
        return mView
    }

    fun initWidgets(mView : View) {
        rvList = mView.rcListMovies
        var mLayoutManager = LinearLayoutManager(activity)
        rvList?.setHasFixedSize(true)
        rvList?.layoutManager = mLayoutManager
        rvList?.isNestedScrollingEnabled = true
        rvList?.itemAnimator = DefaultItemAnimator()

        val divider = DividerItemDecoration(activity, DividerItemDecoration.VERTICAL)
        divider.setDrawable(ContextCompat.getDrawable(activity, R.drawable.item_decoration)!!)
        rvList!!.addItemDecoration(divider)

        adapter = MoviesAdapter().initAdapter(activity, listMoveis!!, activity as MainActivity)
        rvList!!.adapter = adapter
    }

    fun initSwipeRefresh(mView : View) {
        swipeLayout = mView.swpRefreshMovies
        swipeLayout?.setOnRefreshListener {
            swipeLayout!!.isRefreshing = true
            listener!!.refreshList()
        }

        swipeLayout?.setColorSchemeColors(
                ContextCompat.getColor(activity, R.color.colorPrimaryDark),
                ContextCompat.getColor(activity, R.color.colorPrimary),
                ContextCompat.getColor(activity, android.R.color.holo_orange_light))
    }


    open fun setListener(listener: OnRefreshListMoviesListener){
        this.listener = listener
    }

    companion object {
        @JvmStatic
        fun newInstance(listMovie: MutableList<SkyModel>) =
                ListMoviesFragment().apply {
                    arguments = Bundle().apply {
                        putSerializable(Constant.ARG_PARAM_LIST_MOVIE, listMovie as Serializable)
                    }
                }
    }

}