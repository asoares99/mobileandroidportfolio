package br.com.mobiletest.android.util

import android.content.Context
import android.net.ConnectivityManager


class Utils {
    companion object {

        fun checkInternetConnection(context: Context): Boolean {
            var isConnected: Boolean = false

            try {
                val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val networkInfo = connectivityManager.activeNetworkInfo

                if (networkInfo != null && networkInfo.isConnected && networkInfo.isAvailable) {
                    isConnected = true
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return isConnected
        }

    }
}